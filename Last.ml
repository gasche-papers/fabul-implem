type 'a node = 'a Location.loc

type tyvar = string node
type var = string node
type constructor = string node
type type_constructor = string node

type is_rec = Rec | Nonrec

type typ = typ_ node
and typ_ =
  | Var of tyvar
  | Lump of Parsetree.core_type
  | Unit
  | Prod of typ * typ
  | Sum of typ * typ
  | Lolli of typ * typ
  | Bang of typ
  | Box0 | Box1 of typ
  | Constr of type_constructor * typ list


type exp = exp_ node
and exp_ =
  | Var of var
  | Tuple of exp list
  | Let of is_rec * pat * exp * exp
  | Unit
  | Seq of exp * exp
  | Lam of pat * typ * exp
  | App of exp * exp
  | Share of exp
  | Copy of exp
  | Inj of constructor * exp option
  | Match of exp * clause list
  | Fold of exp | Unfold of exp
  | New of exp | Free of exp
  | Box of exp | Unbox of exp
  | LU of (Parsetree.expression * typ option)
and clause = (pat * exp) node
and pat = pat_ node
and pat_ =
  | Any
  | Var of var
  | Unit
  | Tuple of pat list
  | Inj of constructor * pat option
  | Fold of pat
  | Box of pat
  | LU of (Parsetree.pattern * typ option)

type str_item = str_item_ node
and str_item_ =
  | Decl of var * typ
  | Def of is_rec * var * exp
  | Inductive of type_constructor * tyvar list * inductive_clause list
and inductive_clause = (constructor * typ option) node

type sig_item = sig_item_ node
and sig_item_ =
  | Decl of var * typ

type structure = str_item list
type signature = sig_item list
