external reraise : exn -> 'a = "%reraise"

let compile file =
  let open Parsing_lib in
  let inchan = open_in file in
  let lexbuf = Lexing.from_channel inchan in
  Location.init lexbuf file;
  let ast =
    try Parse.implementation lexbuf
    with exn ->
      match Location.error_of_exn exn with
        | None -> reraise exn
        | Some error ->
          Location.report_error Format.err_formatter error;
          exit 1
  in
  Pprintast.structure Format.std_formatter ast;
  Format.pp_print_newline Format.std_formatter ();
  Format.pp_print_flush Format.std_formatter ();
  Format.pp_print_flush Format.err_formatter ();
  ()

module Cmd = struct
  open Cmdliner
  (* external library for command-line handling,
     see http://erratique.ch/software/cmdliner/doc/Cmdliner *)

  let input_file =
    let doc = Arg.info ~docv:"INPUT_FILE" [] in
    Arg.(required (pos 0 (some non_dir_file) None doc))

  let parse () =
    let doc = Term.info "fabul" in
    Term.(eval (input_file, doc))
end

let () =
  match Cmd.parse () with
  | `Ok input_file ->
    compile input_file
  | _ -> ()
