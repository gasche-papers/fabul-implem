type 'a loc = 'a Location.loc = {txt: 'a; loc: Location.t}
open Last
module H = Ast_helper

let todo loc =
  Printf.ksprintf failwith "TODO(%s)" loc

let in_module ~loc mid id =
  {loc; txt = Longident.(Ldot (Lident mid, id))}

let in_runtime ~loc id = in_module ~loc "Lruntime" id
let in_pervasives ~loc id = in_module ~loc "Pervasives" id

let rec_flag is_rec =
  let open Asttypes in
  match is_rec with
  | Rec -> Recursive
  | Nonrec -> Nonrecursive

let apply ~loc f args =
  H.Exp.apply ~loc f (List.map (fun arg -> (Asttypes.Nolabel, arg)) args)

let compile_var : var -> H.lid =
  fun {loc; txt} -> {loc; txt = Longident.Lident txt}

let compile_cons : constructor -> H.lid =
  fun {loc; txt} -> {loc; txt = Longident.Lident txt}

let compile_tycons : type_constructor -> H.lid =
  fun {loc; txt} -> {loc; txt = Longident.Lident txt}

let rec compile_typ ({loc; _} as ty : typ) = match ty.txt with
  | Lump ty -> ty
  | Unit ->
    H.Typ.constr ~loc {loc;txt=Longident.Lident "unit"} []
  | Var {loc;txt=alpha} ->
    H.Typ.var ~loc alpha
  | Prod (ty1, ty2) ->
    H.Typ.tuple ~loc [compile_typ ty1; compile_typ ty2]
  | Lolli (ty1, ty2) ->
    H.Typ.arrow Asttypes.Nolabel (compile_typ ty1) (compile_typ ty2)
  | Bang ty ->
    H.Typ.constr (in_runtime ~loc "bang") [compile_typ ty]
  | Box1 ty ->
    H.Typ.constr (in_runtime ~loc "box1") [compile_typ ty]
  | Constr (tc, tys) ->
    H.Typ.constr ~loc (compile_tycons tc) (List.map compile_typ tys)
  | Box0
  | Sum _ ->
    Format.eprintf "Unsupported type:@.@[%a@]@."
      Lprint.out_type ty;
    todo __LOC__

let unit_pattern ~loc = H.Pat.construct ~loc {loc;txt=Longident.Lident "()"} None

let rec compile_pat ({txt = pat; loc} : pat) = match pat with
  | LU (p, _ty) -> p
  | Unit -> unit_pattern ~loc
  | Any -> H.Pat.any ~loc ()
  | Var v -> H.Pat.var ~loc v
  | Fold p -> compile_pat p
  | Box {txt=Tuple [{txt=Var v}; boxed]} ->
    let boxed =
      let loc = boxed.loc in
      match boxed.txt with
      | Tuple ps -> H.Pat.tuple ~loc (List.map compile_pat ps)
      | _ -> compile_pat boxed in
    H.Pat.alias ~loc boxed v
  | Box _ ->
    Format.eprintf "Unsupported pattern:@.@[%a@]@."
      Lprint.out_pat {loc;txt=pat};
    todo __LOC__
  | Inj (c, Some {txt=Box {txt=Tuple[{txt=Var v}; boxed]}}) ->
    let boxed =
      let loc = boxed.loc in
      match boxed.txt with
      | Tuple ps -> H.Pat.tuple ~loc (List.map compile_pat ps)
      | _ -> compile_pat boxed in
    H.Pat.alias ~loc
      (H.Pat.construct ~loc (compile_cons c) (Some boxed)) v
  | Inj (c, arg) ->
    let arg = match arg with
      | None -> None
      | Some p -> Some (compile_pat p) in
    H.Pat.construct ~loc (compile_cons c) arg
  | Tuple _ ->
    Format.eprintf "Unsupported pattern:@.@[%a@]@."
      Lprint.out_pat {loc;txt=pat};
    todo __LOC__

let rebox ~loc compile_exp l args mk_v =
  (*
     let v = Obj.repr v in
     let arg0 = $arg0 in
     let arg1 = $arg1 in
     ...
     let argN = $argN in
     (* for type-checking purposes *)
     let _ = (fun () -> $l = $mk_v(arg1,..,argN)) in
     set_field (Obj.repr v) 0 (Obj.repr arg0);
     ...
     set_field (Obj.repr v) N (Obj.repr argN);
     v *)
  let pvar ~loc txt = H.Pat.var ~loc {loc;txt} in
  let evar ~loc txt = H.Exp.ident ~loc {loc;txt=Longident.Lident txt} in
  let lname = "l" in
  let argname i = "arg" ^ string_of_int (i + 1) in
  let bindings body =
    let bind_l ({loc} as l) =
      let opaque_identity = in_module ~loc "Sys" "opaque_identity" in
      H.Vb.mk ~loc (pvar ~loc lname)
        (apply ~loc (H.Exp.ident ~loc opaque_identity) [compile_exp l])
    in
    let bind_arg i ({loc} as arg) =
      H.Vb.mk ~loc (pvar ~loc (argname i)) (compile_exp arg) in
    H.Exp.let_ ~loc Asttypes.Nonrecursive
      (bind_l l :: List.mapi bind_arg args)
      body in
  let typing_device =
    let freeze e =
      H.Exp.fun_ ~loc Asttypes.Nolabel None (unit_pattern ~loc) e in
    let test = apply ~loc (H.Exp.ident (in_pervasives ~loc "(=)"))
        [evar ~loc lname;
         mk_v (List.mapi (fun i _ -> evar ~loc (argname i)) args)] in
    freeze test in
  let set_field = H.Exp.ident (in_module ~loc "Obj" "set_field") in
  let repr = H.Exp.ident (in_module ~loc "Obj" "repr") in
  let levar = evar ~loc:l.loc lname in
  let assignments =
    (* set_field (Obj.repr v) i (Obj.repr argi); *)
    let assign i {loc; _} =
      apply ~loc set_field [
        apply ~loc:l.loc repr [levar];
        H.Exp.constant ~loc (H.Const.int i);
        apply ~loc repr [evar ~loc (argname i)];
      ] in
    List.mapi assign args in
  bindings
    (H.Exp.let_ ~loc Asttypes.Nonrecursive
       [H.Vb.mk ~loc (H.Pat.any ~loc ()) typing_device]
       (List.fold_right (H.Exp.sequence ~loc)
          (typing_device :: assignments)
          levar))

let rec compile_exp ({txt = exp; loc} : exp) =
  let runtime_ident ~loc id = H.Exp.ident ~loc (in_runtime ~loc id) in
  match exp with
  | LU (e, _ty) -> e
  | Var v ->
    H.Exp.ident ~loc (compile_var v)
  | Let (is_rec, pat, def, body) ->
    H.Exp.let_ ~loc (rec_flag is_rec)
      [H.Vb.mk (compile_pat pat) (compile_exp def)] (compile_exp body)
  | Unit ->
    H.Exp.construct ~loc {loc; txt=Longident.Lident "()"} None
  | Seq (a, b) ->
    H.Exp.sequence ~loc (compile_exp a) (compile_exp b)
  | Lam (p, ty, e) ->
    H.Exp.fun_ ~loc Asttypes.Nolabel None
      (H.Pat.constraint_ ~loc:ty.loc (compile_pat p) (compile_typ ty))
      (compile_exp e)
  | App (f, arg) ->
    apply ~loc (compile_exp f) [compile_exp arg]
  | Share e ->
    apply ~loc (runtime_ident ~loc "share") [compile_exp e]
  | Copy e ->
    apply ~loc (runtime_ident ~loc "copy") [compile_exp e]
  | Fold e | Unfold e -> compile_exp e (* TODO runtime *)
  | Match (e, clauses) ->
    H.Exp.match_ ~loc (compile_exp e) (compile_clauses clauses)
  | Unbox _
  | New _ | Free _
  | Tuple _
    ->
    Format.eprintf "Unsupported expression:@.@[%a@]@."
      Lprint.out_exp {loc;txt=exp};
    todo __LOC__
  | Box {txt=Tuple [le; boxed]} ->
      begin match boxed.txt with
      | Tuple es ->
        let mk_v es = H.Exp.tuple ~loc:boxed.loc es in
        rebox ~loc compile_exp le es mk_v
      | _ ->
        apply ~loc (runtime_ident ~loc "rebox")
          [compile_exp le; compile_exp boxed]
      end
  | Box ty ->
    Format.eprintf "Unsupported expression:@.@[%a@]@."
      Lprint.out_exp {loc;txt=exp};
    todo __LOC__
  | Inj (c, Some {txt=Box {txt=Tuple [le; boxed]}}) ->
    begin match boxed.txt with
      | Tuple es ->
        let mk_v es =
          let boxed = H.Exp.tuple ~loc es in
          H.Exp.construct (compile_cons c) (Some boxed) in
        rebox ~loc compile_exp le es mk_v
      | _ ->
        apply ~loc (runtime_ident ~loc "rebox")
          [compile_exp le;
           H.Exp.construct (compile_cons c) (Some (compile_exp boxed))]
    end
  | Inj (c, arg) ->
    let arg = match arg with
      | None -> None
      | Some e -> Some (compile_exp e) in
    H.Exp.construct (compile_cons c) arg

and compile_clauses cls = List.map compile_clause cls
and compile_clause {txt=(p, e)} =
  H.Exp.case (compile_pat p) (compile_exp e)

let compile_sig_item {txt; loc} = match (txt : sig_item_) with
  | Decl (x, ty) -> H.Sig.value ~loc (H.Val.mk ~loc x (compile_typ ty))

let compile_signature str = List.map compile_sig_item str

let compile_structure structure =
  let module SMap = Map.Make(String) in
  let process (decls, defs) str_item = match str_item.txt with
    | Inductive _ -> (decls, defs)
    | Def (is_rec, x, exp) ->
      begin match SMap.find x.txt defs with
        | exception Not_found -> ()
        | loc ->
          Location.raise_errorf ~loc:x.loc
            "The identifier %s is already defined in this structure at@\n%a"
            x.txt Location.print loc
      end;
      let defs = SMap.add x.txt str_item.loc defs in
      (decls, defs)
    | Decl (x, typ) ->
      begin match SMap.find x.txt decls with
        | exception Not_found -> ()
        | (loc, _, _) ->
          Location.raise_errorf ~loc:x.loc
            "The identifier %s is already declared in this structure at@\n%a"
            x.txt Location.print loc
      end;
      let decls = SMap.add x.txt (str_item.loc, x, typ) decls in
      (decls, defs)
  in
  (* we will re-traverse the input list to produce the definitions in
     their source code order; "defs" was only used to detect duplicate
     definitions. *)
  let decls, _defs = List.fold_left process (SMap.empty, SMap.empty) structure in
  let do_defs (rev_compiled_defs, rem_decls) {loc; txt} =
    match (txt : Last.str_item_) with
    | Decl _ -> (rev_compiled_defs, rem_decls)
    | Def (is_rec, x, exp) ->
      let x_decl, rem_decls =
        match SMap.find x.txt rem_decls with
        | exception Not_found -> None, rem_decls
        | (loc, _x, typ) -> Some (loc, typ), SMap.remove x.txt rem_decls in
      let value_binding =
        let pat = H.Pat.var x in
        let pat = match x_decl with
          | None -> pat
          | Some (loc, typ) -> H.Pat.constraint_ ~loc pat (compile_typ typ) in
        H.Vb.mk ~loc pat (compile_exp exp)
      in
      (H.Str.value (rec_flag is_rec) [value_binding] :: rev_compiled_defs,
       rem_decls)
    | Inductive (tc, params, clauses) ->
      let compile_param {loc; txt = alpha} =
        (H.Typ.var ~loc alpha, Asttypes.Invariant) in
      let params = List.map compile_param params in
      let compile_clause {loc; txt = (c, ty)} =
        let args = match ty with
          | None -> None
          | Some ty ->
            let args = match ty with
              | {txt=Box1 boxed} ->
                let rec args boxed = match boxed.txt with
                  | Prod (ty, tys) -> compile_typ ty :: args tys
                  | _ -> [compile_typ boxed]
                in args boxed
              | _ -> [compile_typ ty] in
            Some (Parsetree.Pcstr_tuple args) in
        H.Type.constructor ~loc ?args c in
      let kind =
        Parsetree.Ptype_variant (List.map compile_clause clauses) in
      let tydecl =
        H.Str.type_ ~loc Asttypes.Recursive
          [H.Type.mk ~loc ~params ~kind tc] in
      (tydecl :: rev_compiled_defs, rem_decls)
  in
  let rev_compiled_defs, rem_decls = List.fold_left do_defs ([], decls) structure in
  begin match SMap.choose rem_decls with
    | exception Not_found -> ()
    | (_, (loc, x, _typ)) ->
      Location.raise_errorf ~loc
        "The identifier %s is declared but not defined in this structure"
        x.txt
  end;
  List.rev rev_compiled_defs
