type 'a loc = 'a Location.loc = {txt: 'a; loc: Location.t}
open Last

let rec comma_separated pr ppf = function
| [] -> ()
| [x] -> pr ppf x
| x :: xs -> Format.fprintf ppf "%a, " pr x; comma_separated pr ppf xs

let rec out_type ppf (ty : typ) =
  let (!) ppf fmt = Format.fprintf ppf fmt in
  let rec out_type ppf ty = out_lolli ppf ty
  and out_lolli ppf (ty : Last.typ) = match ty.txt with
  | Lolli (ty1, ty2) -> !ppf "%a -o %a" out_prod ty1 out_lolli ty2
  | _ -> out_prod ppf ty
  and out_prod ppf (ty : Last.typ) = match ty.txt with
  | Prod (ty1, ty2) -> !ppf "%a * %a" out_sum ty1 out_prod ty2
  | _ -> out_sum ppf ty
  and out_sum ppf (ty : Last.typ) = match ty.txt with
  | Sum (ty1, ty2) -> !ppf "%a + %a" out_simple ty1 out_sum ty2
  | _ -> out_simple ppf ty
  and out_simple ppf (ty : Last.typ) = match ty.txt with
  | Var alpha -> !ppf "'%s" alpha.txt
  | Lump ml_type -> !ppf "[%a]" Pprintast.core_type ml_type
  | Unit -> !ppf "1"
  | Bang ty -> !ppf "!%a" out_simple ty
  | Box0 -> !ppf "box0"
  | Box1 ty -> !ppf "box1 %a" out_simple ty
  | Constr (c, tys) ->
    !ppf "%s" c.txt;
    List.iter (fun ty -> !ppf " %a" out_simple ty) tys;
  | (Lolli _ | Prod _ | Sum _) -> !ppf "(%a)" out_type ty
  in !ppf "@[%a@]" out_type ty

let out_is_rec ppf = function
  | Rec -> Format.fprintf ppf "rec "
  | Nonrec -> ()

let rec out_pat ppf (p : pat) =
  let (!) ppf fmt = Format.fprintf ppf fmt in
  let rec out_pat ppf (p : pat) = match p.txt with
    | Any -> !ppf "_"
    | Var v -> !ppf "%s" v.txt
    | Unit -> !ppf "()"
    | Tuple ps -> !ppf "(%a)" (comma_separated out_pat) ps
    | Inj (c, None) -> !ppf "%s" c.txt
    | Inj (c, Some p) -> !ppf "%s %a" c.txt out_pat p
    | Fold p -> !ppf "(fold %a)" out_pat p
    | Box p -> !ppf "(box %a)" out_pat p
    | LU (p, None) -> !ppf "(%%U %a)" Pprintast.pattern p
    | LU (p, Some ty) -> !ppf "(%%U %a :> %a)" Pprintast.pattern p out_type ty
  in out_pat ppf p

let rec out_exp ppf (e : exp) =
  let (!) ppf fmt = Format.fprintf ppf fmt in
  let rec out_exp ppf (e : exp) = match e.txt with
    | Var v -> !ppf "%s" v.txt
    | Tuple es -> !ppf "(%a)" (comma_separated out_exp) es
    | Let (is_rec, p, def, body) ->
      !ppf "let %a%a = %a in %a"
        out_is_rec is_rec out_pat p out_exp def out_exp body
    | Unit -> !ppf "()"
    | Seq (e1, e2) -> !ppf "(%a); %a" out_exp e1 out_exp e2
    | Lam (p, ty, e) -> !ppf "fun %a : %a -> %a" out_pat p out_type ty out_exp e
    | App (f, arg) -> !ppf "(%a) %a" out_exp f out_exp arg
    | Share e -> !ppf "share %a" out_exp e
    | Copy e -> !ppf "copy %a" out_exp e
    | Inj (tc, None) -> !ppf "%s" tc.txt
    | Inj (tc, Some e) -> !ppf "%s %a" tc.txt out_exp e
    | Match (e, cs) ->
      let out_clause ppf {txt = (p, e)} = !ppf "| %a -> %a" out_pat p out_exp e in
      let out_clauses ppf clauses = List.iter (out_clause ppf) clauses in
      !ppf "match (%a) with %a" out_exp e out_clauses cs
    | Fold e -> !ppf "fold %a" out_exp e
    | Unfold e -> !ppf "unfold %a" out_exp e
    | New e -> !ppf "new %a" out_exp e
    | Free e -> !ppf "free %a" out_exp e
    | Box e -> !ppf "box %a" out_exp e
    | Unbox e -> !ppf "unbox %a" out_exp e
    | LU (e, None) -> !ppf "(%%U %a)" Pprintast.expression e
    | LU (e, Some ty) -> !ppf "(%%U %a :> %a)" Pprintast.expression e out_type ty
  in out_exp ppf e
