type 'a loc = 'a Location.loc = {txt: 'a; loc: Location.t}
open Last

type 'a dict = (string * 'a) list

type env = {
  env: typ dict;
  used: Location.t dict;
}

let empty_env = {
  env = [];
  used = [];
}

let duplicable (ty : typ) = match ty.txt with
  | Bang _ -> true
  | _ -> false

let left {env; used} =
  List.filter (fun (x, _ty) -> not (List.mem_assoc x used)) env

let rec ty_eq (ty1 : Last.typ) (ty2 : Last.typ) =
  match ty1.txt, ty2.txt with
  | Var alpha1, Var alpha2 -> (alpha1.txt = alpha2.txt)
  | (Var _, _) | (_, Var _) -> false
  | Lolli (ta1, tb1), Lolli (ta2, tb2)
  | Sum (ta1, tb1), Sum (ta2, tb2)
  | Prod (ta1, tb1), Prod (ta2, tb2) ->
    ty_eq ta1 tb1 && ty_eq ta2 tb2
  | (Lolli _ | Sum _ | Prod _), _ | _, (Lolli _ | Sum _ | Prod _) -> false
  | Bang ty1, Bang ty2
  | Box1 ty1, Box1 ty2 -> ty_eq ty1 ty2
  | Lump ty1, Lump ty2 ->
    (* TODO keep (ty1=ty2) constraint *)
    true
  | (Bang _ | Box1 _ | Lump _), _ | _, (Bang _ | Box1 _ | Lump _) -> false
  | Constr (tc1, tys1), Constr (tc2, tys2) ->
    (tc1.txt = tc2.txt) && (try List.for_all2 ty_eq tys1 tys2 with _ -> false)
  | Constr _, _ | _, Constr _ -> false
  | Unit, Unit -> true
  | Box0, Box0 -> true
  | (Box0 | Unit), _ (* | _, (Box0 | Unit) *) -> false

let check_ty_eq loc ty1 ty2 =
  (* TODO refine with U constraints *)
  if not (ty_eq ty1 ty2) then
    Location.raise_errorf ~loc
      "Type error: type mismatch, incompatible types %a and %a@."
      Lprint.out_type ty1 Lprint.out_type ty2;
  ()

let merge_ty loc ty1 ty2 = check_ty_eq loc ty1 ty2; ty1
let merge_env (loc1, env1) (loc2, env2) =
  let used env = List.map fst env.used in
  if env1.env <> env2.env then invalid_arg "merge_env";
  let used1, used2 = used env1, used env2 in
  let only1 = List.filter (fun x -> not (List.mem x used2)) used1 in
  let only2 = List.filter (fun x -> not (List.mem x used1)) used2 in
  begin match only1, only2 with
  | [], [] -> assert (used1 = used2); (loc2, env2)
  | x::_, _ ->
    Location.raise_errorf ~loc:(List.assoc x env1.used)
      "Type error: Variable %s is not consumed in @.%a"
        x Location.print loc1
  | _, x::_ ->
    Location.raise_errorf ~loc:(List.assoc x env2.used)
      "Type error: Variable %s is not consumed in @.%a"
        x Location.print loc2
  end

let pop_from_env env vars =
  let pop env {loc; txt=x} =
    begin match List.assoc x env.env with
      | exception Not_found ->
        invalid_arg "pop_from_env"
      | ty ->
        if not (duplicable ty) && not (List.mem_assoc x env.used) then
          Location.raise_errorf ~loc
            "Type error: the variable %s was never consumed.@." x
    end;
    let rem dict = List.remove_assoc x dict in
    { env = rem env.env; used = rem env.used; }
  in List.fold_left pop env vars

let rec typecheck (env : env) ({loc;txt} : exp) : env * typ = match txt with
  | Var x ->
    begin match List.assoc x.txt env.used with
      | used_loc ->
        Location.raise_errorf ~loc "Type error: the linear variable %s
          was already used at@.%a@." x.txt Location.print used_loc
      | exception Not_found ->
        begin match List.assoc x.txt env.env with
        | exception Not_found ->
          (* TODO: weaken this as cross-boundary bindings may
             be lost (at duplicable types) *)
          Location.raise_errorf ~loc "Type error: unbound variable %s@." x.txt
        | ty ->
          ((if duplicable ty then env
           else { env with used = (x.txt, x.loc) :: env.used }),
           ty)
        end
    end
  | Unit -> (env, {loc;txt=Unit})
  | Tuple es ->
    begin match es with
      | [e1; e2] ->
        let (env, ty1) = typecheck env e1 in
        let (env, ty2) = typecheck env e2 in
        (env, {loc; txt = Prod (ty1, ty2)})
      | _ -> failwith "TODO"
    end
  | Let (is_rec, p, def, body) ->
    if is_rec = Rec then failwith "TODO";
    let env, def_ty = typecheck env def in
    let env, to_pop = typecheck_pat env p def_ty in
    let env, ty = typecheck env body in
    let env = pop_from_env env to_pop in
    (env, ty)
  | Seq (e1, e2) ->
    typecheck env {loc; txt=Let (Nonrec, {loc;txt=Unit}, e1, e2)}
  | Lam (p, p_ty, e) ->
    let env, to_pop = typecheck_pat env p p_ty in
    let env, ty = typecheck env e in
    let env = pop_from_env env to_pop in
    (env, {loc; txt = Lolli (p_ty, ty)})
  | App (f, arg) ->
    let env, f_ty = typecheck env f in
    let env, arg_ty = typecheck env arg in
    begin match f_ty with
      | {txt = Lolli (in_ty, out_ty)} ->
        check_ty_eq loc arg_ty out_ty;
        (env, out_ty)
      | _ -> Location.raise_errorf ~loc:f.loc
               "Type error: a function type is expected at application point.@."
    end
  | Share e ->
    let env, ty = typecheck env e in
    (env, {loc; txt = Bang ty})
  | Copy e ->
    let env, ty = typecheck env e in
    begin match ty with
      | {txt = Bang ty} -> (env, ty)
      | _ -> Location.raise_errorf ~loc:e.loc
               "Type error: a duplicable type is expected at copy point.@."
    end
  | Inj _ -> failwith "TODO"
  | Match (e, clauses) ->
    let env, ty = typecheck env e in
    let check_clause env {loc; txt = (p, e)} ty =
      let env, to_pop = typecheck_pat env p ty in
      let env, ty = typecheck env e in
      let env = pop_from_env env to_pop in
      ((loc, env), ty)
    in
    let results = List.map (fun p -> check_clause env p ty) clauses in
    let envs, tys = List.split results in
    let (_, env), ty = List.hd envs, List.hd tys in
    ignore (List.fold_left (merge_ty loc) ty tys : typ);
    ignore (List.fold_left merge_env (List.hd envs) envs : Location.t * env);
    (env, ty)
  | New e ->
    let env, ty = typecheck env e in
    check_ty_eq loc ty {loc;txt=Unit};
    failwith "TODO"
  | Free e ->
    let env, ty = typecheck env e in
    begin match ty with
      | {txt=Box1 ty} -> (env, {loc;txt=Box0})
      | _ -> Location.raise_errorf ~loc:e.loc
               "Type error: a box1 type is expected a free point.@."
    end
  | Box e ->
    let env, ty = typecheck env e in
    begin match ty with
      | {txt=Prod({txt=Box0},ty)} -> env, {loc; txt=Box1 ty}
      | _ -> Location.raise_errorf ~loc:e.loc
               "Type error: (box0 * ty) expected at box point.@."
    end
  | Unbox e ->
    let env, ty = typecheck env e in
    begin match ty with
      | {txt=Box1 ty} -> env, {loc;txt=Prod({loc;txt=Box0},ty)}
      | _ -> Location.raise_errorf ~loc:e.loc
               "Type error: (ty box1) expected at box point.@."
    end
  | Fold _ | Unfold _ -> failwith "TODO"
  | LU e -> failwith "TODO"

and typecheck_pat env p ty : env * var list =
  let rec typecheck_pat env ({loc; txt} : pat) ty acc = match txt with
    | Any ->
      if duplicable ty then (env, acc)
      else Location.raise_errorf ~loc
          "Type error: this pattern ignores a non-duplicable value@."
    | Var x -> ({ env with env = (x.txt, ty) :: env.env }, x::acc)
    | Unit -> (env, acc)
    | Tuple ps ->
      let rec check env ps ty acc = match ps with
        | [] -> assert false
        | [p] -> typecheck_pat env p ty acc
        | p::ps ->
          begin match ty with
            | {txt = Prod (ty, tyrest); _} ->
              let (env, acc) = typecheck_pat env p ty acc in
              check env ps tyrest acc
            | _ -> Location.raise_errorf ~loc
                     "Type error: a product type was expect for a tuple pattern.@."
          end
      in check env ps ty acc
    | Inj _ -> failwith "TODO"
    | Fold _ -> failwith "TODO"
    | Box p ->
      begin match ty with
        | {txt = Box1 ty} -> typecheck_pat env p ty acc
        | _ -> Location.raise_errorf ~loc
                 "Type error: a box pattern expects a box1 type.@."
      end
    | LU p ->
      begin match ty with
        | {txt = Bang {txt = Lump ty}} ->
          (* TODO: record typing constraint against this location? *)
          (env, acc)
        | _ -> Location.raise_errorf ~loc
                 "Type error: U patterns may only appear at ![..] types.@."
      end
  in typecheck_pat env p ty []
