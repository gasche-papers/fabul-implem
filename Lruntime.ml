type 'a bang = Bang of 'a [@@unboxed]
type 'a box1 = Box of 'a [@@unboxed]

let share x = Bang x
let copy (Bang x) = x

